package ro.infoiasi.min.domain;

import java.util.ArrayList;
import java.util.List;

public class Colony {
    private List<Ant> ants;

    public Colony(int numberOfAnts) {
        ants = new ArrayList<>(numberOfAnts);
        for (int i = 0; i < numberOfAnts; i++) {
            ants.add(new Ant());
        }
    }

    public List<Ant> getAnts() {
        return ants;
    }

    public void setAnts(List<Ant> ants) {
        this.ants = ants;
    }
}
