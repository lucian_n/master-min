package ro.infoiasi.min.domain;

import java.util.Map;
import java.util.Set;

public class SCP {
    private Universe universe;
    private Sets sets;
    private Map<Set, Integer> setDictionary;

    public Universe getUniverse() {
        return universe;
    }

    public void setUniverse(Universe universe) {
        this.universe = universe;
    }

    public Sets getSets() {
        return sets;
    }

    public void setSets(Sets sets) {
        this.sets = sets;
    }

    public Map<Set, Integer> getSetDictionary() {
        return setDictionary;
    }

    public void setSetDictionary(Map<Set, Integer> setDictionary) {
        this.setDictionary = setDictionary;
    }
}
