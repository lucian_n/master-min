package ro.infoiasi.min.domain;

import java.util.HashSet;
import java.util.Set;

public class Solution {
    private Set<Set<Integer>> solution = new HashSet<>();

    public Set<Set<Integer>> getSolution() {
        return solution;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Solution solution1 = (Solution) o;

        return solution != null ? solution.equals(solution1.solution) : solution1.solution == null;
    }

    @Override
    public int hashCode() {
        return solution != null ? solution.hashCode() : 0;
    }

    @Override
    public String toString() {
        return solution.toString();
    }
}
