package ro.infoiasi.min.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Sets {

    List<Set<Integer>> sets = new ArrayList<>();

    public List<Set<Integer>> getSets() {
        return sets;
    }

    public void setSets(List<Set<Integer>> sets) {
        this.sets = sets;
    }
}
