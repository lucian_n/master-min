package ro.infoiasi.min.domain;

import java.util.Set;

public class Universe {

    Set<Integer> values;

    public Set<Integer> getValues() {
        return values;
    }

    public void setValues(Set<Integer> values) {
        this.values = values;
    }
}
