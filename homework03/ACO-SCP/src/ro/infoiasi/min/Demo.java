package ro.infoiasi.min;


import ro.infoiasi.min.algorithm.ACO;
import ro.infoiasi.min.domain.SCP;
import ro.infoiasi.min.domain.Sets;
import ro.infoiasi.min.domain.Solution;
import ro.infoiasi.min.domain.Universe;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Demo {
    public static void main(String[] args) throws IOException {
        SCP scp = new SCP();
        Universe universe = new Universe();
       /* Integer[] universeValues = new Integer[]{1,2,3,4};
        universe.setValues(new HashSet<>(Arrays.asList(universeValues)));*/
    /*    Integer[] set1 = new Integer[]{1,4};
        Integer[] set2 = new Integer[]{1,3,4};
        Integer[] set3 = new Integer[]{3,2};*/
        Sets sets = new Sets();
        Map<Set, Integer> setDictionary = new HashMap<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader("data.in"));
        String line;
        boolean first = true;
        int counter = 0;
        while ((line = bufferedReader.readLine()) != null) {
            if(first){
                Set<Integer> universValuesSet = new HashSet<>();
                for(String value : line.split(" ")) {
                    universValuesSet.add(Integer.parseInt(value));
                }
                universe.setValues(universValuesSet);
                first = false;
                continue;
            }

            String[] values = line.split(" ");
            Set<Integer> valuesSet = new HashSet<>();
            for (int i = 1; i < values.length; i++) {
                valuesSet.add(Integer.parseInt(values[i]));
            }
            sets.getSets().add(valuesSet);
            setDictionary.put(valuesSet, counter);
            counter++;
        }

        /*sets.getSets().add(new HashSet<>(Arrays.asList(set1)));
        sets.getSets().add(new HashSet<>(Arrays.asList(set2)));
        sets.getSets().add(new HashSet<>(Arrays.asList(set3)));*/
        scp.setUniverse(universe);
        scp.setSets(sets);
        scp.setSetDictionary(setDictionary);
      /*  for(Set set : sets.getSets()) {
            System.out.println(set.size());
        }*/
        ACO aco = new ACO();
        Solution solution = aco.run(scp);
        System.out.println(solution.getSolution());

    }
}
