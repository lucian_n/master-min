package ro.infoiasi.min.config;


public class Configuration {

    public static final int NUMBER_OF_ANTS = 100;
    public static final double EVAPORATION_RATE = 0.1;
    public static final double ALFA = 1.3;
    public static final double BETA = 1;
    public static final double DIVISION_RATE = 10;
}
