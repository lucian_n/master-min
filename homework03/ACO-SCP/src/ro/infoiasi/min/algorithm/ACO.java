package ro.infoiasi.min.algorithm;

import ro.infoiasi.min.domain.Ant;
import ro.infoiasi.min.domain.Colony;
import ro.infoiasi.min.domain.SCP;
import ro.infoiasi.min.domain.Solution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import static java.lang.Math.pow;
import static ro.infoiasi.min.config.Configuration.ALFA;
import static ro.infoiasi.min.config.Configuration.BETA;
import static ro.infoiasi.min.config.Configuration.DIVISION_RATE;
import static ro.infoiasi.min.config.Configuration.EVAPORATION_RATE;
import static ro.infoiasi.min.config.Configuration.NUMBER_OF_ANTS;

public class ACO {

    private Map<Set, Double> pheromones;
    private Colony colony;
    private Solution bestSolution;

    public Solution run(SCP scp) {
        initialization(scp);
        int t = 0;
        while (t < 500) {
            t++;
            Map<Set<Integer>, Integer> solutionPopularity = new HashMap<>();
            List<Solution> solutions = new ArrayList<>();
            for (Ant ant : colony.getAnts()) {
                Solution solution = constructSolution(scp);
                solutions.add(solution);
            }
            List<Double> fitnessScores = new ArrayList<>();
            if (bestSolution == null) {
                bestSolution = solutions.get(0);
            }
            int sumOfSizes = 0;
            for (Solution solution : solutions) {
                int universeSize = scp.getUniverse().getValues().size();
                int solutionSize = solution.getSolution().size();
                sumOfSizes += solutionSize;
                fitnessScores.add((universeSize - solutionSize)*1./(universeSize*DIVISION_RATE));
                if(bestSolution.getSolution().size() > solutionSize) {
                    bestSolution = solution;
                }
                Set<Integer> solutionI = new HashSet<>();
                for(Set set : solution.getSolution()) {
                    solutionI.add(scp.getSetDictionary().get(set));
                }
                Integer popularity = solutionPopularity.get(solutionI);
                popularity = popularity == null ? 1 : popularity + 1;
                solutionPopularity.put(solutionI, popularity);
            }
            //*********************** add best score to the solution set
            int universeSize = scp.getUniverse().getValues().size();
            fitnessScores.add((universeSize - bestSolution.getSolution().size())*1./(universeSize*DIVISION_RATE));
            solutions.add(bestSolution);
            for(Set set : scp.getSets().getSets()) {
                double sumOfFitness = 0;
                int i= 0;
                for (Solution solution : solutions) {
                    if(solution.getSolution().contains(set)) {
                        sumOfFitness+=fitnessScores.get(i);
                    }
                    i++;
                }
                double pheromone = pheromones.get(set);
                pheromone = (1-EVAPORATION_RATE) * pheromone + EVAPORATION_RATE * sumOfFitness;
                pheromones.put(set, pheromone);
            }
            System.out.println("generation "+t);
            System.out.println("\tbest-size: " + sumOfSizes/(1.* colony.getAnts().size()) +" - " + bestSolution.getSolution().size());
            for (Map.Entry<Set<Integer>, Integer> entry : solutionPopularity.entrySet()) {
                if (entry.getValue() > 10)
                System.out.println("\t" + entry.getValue() + " - " +entry.getKey().size()+ " - " +entry.getKey());
                if(t==500) {
                    Set<Integer> duplicates = new HashSet<>();
                    int duplicatesNo = 0;
                    if(entry.getValue() > 50) {
                        Set<Integer> keys = entry.getKey();
                        for(int key : keys) {
                            if(duplicates.contains(scp.getSetDictionary().get(key))) {
                                duplicatesNo++;
                            }
                            else {
                                duplicates.add(scp.getSetDictionary().get(key));
                            }
                        }
                        System.out.println("Duplicates : " + duplicatesNo);
                    }
                }
            }
        }

        return bestSolution;

    }

    private Solution constructSolution(SCP scp) {
        Solution solution = new Solution();
        Set<Integer> unvisitedUnivers = new HashSet<>(scp.getUniverse().getValues());
        List<Set> unvisitedSets = new ArrayList<>(scp.getSets().getSets());
        while (!unvisitedUnivers.isEmpty()) {
            int element = chooseRandomElement(unvisitedUnivers);
            List<Set<Integer>> neighborhood = getNeighborhood(unvisitedSets, element, solution);
            List<Double> probabilities = generateProbabilities(neighborhood, unvisitedUnivers, solution);
            Set<Integer> selectedSet = selectSet(probabilities, neighborhood);
            solution.getSolution().add(selectedSet);
            unvisitedUnivers.removeAll(selectedSet);
            unvisitedSets.remove(selectedSet);
        }
        return solution;
    }

    private Set<Integer> selectSet(List<Double> probabilities, List<Set<Integer>> neighborhood) {
        double random = new Random().nextDouble();
        double sum = 0;
        int i = 0;
        for(Double probability : probabilities) {
            sum+=probability;
            if(random < sum) {
                return neighborhood.get(i);
            }
            i++;
        }
        return null;
    }

    private List<Double> generateProbabilities(List<Set<Integer>> neighborhood, Set<Integer> unvisitedUniverse, Solution solution) {
        List<Double> probabilities = new ArrayList<>();
        double sum = 0;
        for (Set<Integer> set : neighborhood) {
            int additionallyVertices = 0;
            int duplicates = 0;
            for(Integer number : set) {
                if (unvisitedUniverse.contains(number)) {
                    additionallyVertices++;
                }
                for(Set<Integer> set1 : solution.getSolution()) {
                    if(set1.contains(number)) {
                        duplicates++;
                        break;
                    }
                }
            }

            double pheromone = pheromones.get(set);
            sum += pow(pheromone,ALFA)*pow(additionallyVertices /*+ 50 - duplicates*/, BETA);
            probabilities.add(pow(pheromone,ALFA)*pow(additionallyVertices /*+ 50 -duplicates*/, BETA));
        }
        for (int i = 0; i < probabilities.size(); i++) {
            probabilities.set(i, probabilities.get(i)/sum);
        }
        return probabilities;
    }

    private List<Set<Integer>> getNeighborhood(List<Set> sets, int element, Solution solution) {
        List<Set<Integer>> neighborhood = new ArrayList<>();
        for (Set<Integer> set : sets) {
            if(set.contains(element)) {
                neighborhood.add(set);
            }
        }
        return neighborhood;
    }

    private int chooseRandomElement(Set<Integer> unvisitedUnivers) {
        List<Integer> elements = new ArrayList<>(unvisitedUnivers);
        Collections.shuffle(elements);
        return elements.get(0);
    }

    private void initialization(SCP scp) {
        pheromones = new HashMap<>();
        for (Set set : scp.getSets().getSets()) {
            pheromones.put(set, 0.1);
        }
        colony = new Colony(NUMBER_OF_ANTS);
    }
}
