package ro.infoiasi.min.homework01.hc.model.functions;

import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.ProblemParameters;

import static ro.infoiasi.min.homework01.hc.configuration.Configuration.AVOID_ZERO_DIVIDE_EPSILON;
import static ro.infoiasi.min.homework01.hc.util.Utils.toPrecision;

/**
 * Created by lucian on 04.03.2017.
 */
public class GriewangkFunction extends Function {

    public GriewangkFunction(int domainDimension) {
        problemParameters = new ProblemParameters(domainDimension, 600, -600, AVOID_ZERO_DIVIDE_EPSILON);
    }

    public double evaluate(double[] input) {
        double result = 0;
        for (int i = 0; i < input.length; i++) {
            result += Math.pow(input[i], 2)/4000;
        }
        double temp = 1;
        for (int i = 0; i < input.length; i++) {
            temp *= Math.cos(input[i]/Math.sqrt(i+1.));
        }
        result -= temp;
        //return toPrecision(result + 1);
        return result + 1;
    }
}
