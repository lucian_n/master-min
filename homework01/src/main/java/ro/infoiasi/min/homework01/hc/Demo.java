package ro.infoiasi.min.homework01.hc;

import ro.infoiasi.min.homework01.hc.algorithm.HC;
import ro.infoiasi.min.homework01.hc.algorithm.impl.BestImproverImpl;
import ro.infoiasi.min.homework01.hc.algorithm.impl.FirstImproverImpl;
import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.Solution;
import ro.infoiasi.min.homework01.hc.model.functions.GriewangkFunction;
import ro.infoiasi.min.homework01.hc.model.functions.RastriginFunction;
import ro.infoiasi.min.homework01.hc.model.functions.RosenbrockFunction;
import ro.infoiasi.min.homework01.hc.model.functions.SixHumpCamelBackFunction;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.DoubleStream;

import static ro.infoiasi.min.homework01.hc.configuration.Configuration.HC_NUMBER_OF_ITERATIONS;
import static ro.infoiasi.min.homework01.hc.configuration.Configuration.precision;
import static ro.infoiasi.min.homework01.hc.util.Utils.generateRandomCandidate;
import static ro.infoiasi.min.homework01.hc.util.Utils.getPhenotypeFrom;

/**
 * Created by lnevoe on 3/3/2017.
 */
public class Demo {

    public static void main(String[] args) {
        Function rastriginFunction = new RastriginFunction(30);
        Function griewangkFunction = new GriewangkFunction(2);
        Function rosenbrockFunction = new RosenbrockFunction(2);
        Function sixHumpCamelBackFunction = new SixHumpCamelBackFunction();
        HC hcInstance = new HC(new FirstImproverImpl(false));

        runIteratedHc(hcInstance, rastriginFunction, HC_NUMBER_OF_ITERATIONS);
        Solution rastriginFunctionCandidate = generateRandomCandidate(rastriginFunction.getProblemParameters());
        File rastriginFirstImprovementSteps = new File("rastriginFirstImprovementSteps.data");
        File rastriginBestImprovementSteps = new File("rastriginBestImprovementSteps.data");
       // writeStatistics(new HC(new FirstImproverImpl()), rastriginFunction, rastriginFunctionCandidate, rastriginFirstImprovementSteps, true);
       /* writeStatistics(new HC(new FirstImproverImpl(false)), rastriginFunction, rastriginFunctionCandidate, rastriginFirstImprovementSteps, true);
        writeStatistics(new HC(new BestImproverImpl(false)), rastriginFunction, rastriginFunctionCandidate, rastriginBestImprovementSteps, false);
*/
    }

    private static void writeStatistics(HC hc, Function function, Solution candidate, File file, boolean firstImp) {
        SortedMap<Integer, Double> result = new TreeMap<>();
        if(!firstImp) {
            hc.run(function, candidate);
            result = hc.getBestGenerationScores();
        }
        else {
            SortedMap<Integer, List<Double>> firstScores  = new TreeMap<>();
            for (int i = 0; i < 1; i++) {
                hc.run(function, candidate);
                SortedMap<Integer, Double> bestGenerationScores = hc.getBestGenerationScores();
                for(Map.Entry<Integer, Double> entry : bestGenerationScores.entrySet()) {
                    int index = entry.getKey();// - entry.getKey() % 5;
                    firstScores.putIfAbsent(index, new ArrayList<>());
                    firstScores.get(index).add(entry.getValue());
                }
            }
            for(Map.Entry<Integer, List<Double>> entry : firstScores.entrySet()) {
                double avg = entry.getValue().stream().mapToDouble(Double::doubleValue).sum();
                avg /= entry.getValue().size();
                result.put(entry.getKey(), avg);
            }
        }

        try {
            BufferedWriter fileWriter = new BufferedWriter(new FileWriter(file));
            for(Map.Entry<Integer, Double> entry : result.entrySet()) {
                fileWriter.write(entry.getKey() + " " + entry.getValue());
                fileWriter.newLine();
            }
            fileWriter.flush();
            fileWriter.close();
            System.out.println(file.getAbsoluteFile() + "...done");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void runIteratedHc(HC hcInstance, Function function, int iterations) {
        double bestScore = Double.MAX_VALUE;
        List<Integer> curentScores = new ArrayList<>();
        for (int i = 0; i < iterations; i++) {
            Solution solution = hcInstance.run(function, generateRandomCandidate(function.getProblemParameters()));
            double currentScore = function.evaluate(getPhenotypeFrom(solution, function.getProblemParameters()));
            System.out.println(currentScore);
            if(bestScore > currentScore) {
                bestScore = currentScore;
                System.out.println("Iteratia: " + i);
                /*for(double input : getPhenotypeFrom(solution, function.getProblemParameters())) {
                    //BigDecimal bigDecimal = new BigDecimal(input);
                    //System.out.print(bigDecimal.setScale(precision, RoundingMode.HALF_UP)+ " , ");
                }
                System.out.println("\n-----------------------------------------------------------------------------------------");*/

            }
        }
        System.out.println("best score : " + bestScore);

    }
}
