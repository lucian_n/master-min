package ro.infoiasi.min.homework01.ga.algorithm;

import ro.infoiasi.min.homework01.ga.fitness.FitnessFunction;
import ro.infoiasi.min.homework01.ga.model.GAResolution;
import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.ProblemParameters;
import ro.infoiasi.min.homework01.hc.model.Solution;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import static ro.infoiasi.min.homework01.hc.configuration.Configuration.*;
import static ro.infoiasi.min.homework01.hc.util.Utils.generateRandomCandidate;
import static ro.infoiasi.min.homework01.hc.util.Utils.getPhenotypeFrom;

/**
 * Created by lucian on 05.03.2017.
 */
public class GA {

    protected FitnessFunction fitnessFunction;
    protected SelectionAlgorithm selectionAlgorithm;
    protected CrossoverAlgorithm crossoverAlgorithm;

    public GA(FitnessFunction fitnessFunction, SelectionAlgorithm selectionAlgorithm, CrossoverAlgorithm crossoverAlgorithm) {
        this.fitnessFunction = fitnessFunction;
        this.selectionAlgorithm = selectionAlgorithm;
        this.crossoverAlgorithm = crossoverAlgorithm;
    }

    public GAResolution run(Function function) {
        int generation = 0;
        GAResolution gaResolution = new GAResolution();
        List<Solution> population = initialization(POPULATION_SIZE, function.getProblemParameters());
        List<Double> fitnessScores = evaluation(population, function, gaResolution);
        while (!converge(population) && generation < MAX_GENERATION_NUMBER) {
            generation ++;
            population = selectionAlgorithm.select(population, fitnessScores);
            mutation(population, MUTATION_PROBABILITY);
            crossoverAlgorithm.crossover(population, CROSS_OVER_PROBABILITY);
            fitnessScores = evaluation(population, function, gaResolution);

            logStatistics(gaResolution, generation, function);
        }

        return gaResolution;
    }

    protected void logStatistics(GAResolution gaResolution, int generation, Function function) {
        System.out.println("\t\tGeneratia "+generation+": ");
        System.out.println("\t\t\tScore avg: " + gaResolution.getScoreAvg().get(gaResolution.getScoreAvg().size()-1));
      //  System.out.println("\t\t\tBest Fitness score: " + fitnessFunction.calculateScore(function, getPhenotypeFrom(gaResolution.getBestSolutions().get(generation - 1), function.getProblemParameters())));
        System.out.println("\t\t\tBest Score: " + function.evaluate(getPhenotypeFrom(gaResolution.getBestSolutions().get(gaResolution.getBestSolutions().size()-1), function.getProblemParameters())));
    }

    protected void logPopulation(int generation, List<Solution> population) {
        System.out.println("Generatia "+generation+": ");
        System.out.println("\tPopulatie:");
        int i = 1;
        for (Solution specimen : population) {
            System.out.println("\t\t" + i + ": " + specimen);
            i++;
        }
    }

    protected boolean converge(List<Solution> population) {
        Solution standard = population.get(0);
        for (int i = 1; i < population.size(); i++) {
            if(!standard.getGenotype().equals(population.get(i).getGenotype())) {
                return false;
            }
        }
        return true;
    }

/*
    private boolean converge(List<Double> fitnessScores) {
        for (int i = 0; i < fitnessScores.size() - 1; i++) {
            if(!fitnessScores.get(i).equals(fitnessScores.get(i+1))){
                return false;
            }
        }
        return true;
    }
*/

    protected void mutation(List<Solution> population, double mutationProbability) {
        int c = 0;
        for (Solution specimen : population) {
            BitSet chromosome = specimen.getGenotype();
            int n = IntStream.of(specimen.getNs()).sum();
           // double mutationProbability = 1./n;
            for (int i = 0; i < n; i++) {
                double randomValue = ThreadLocalRandom.current().nextDouble(0, 1);
                if(randomValue < mutationProbability) {
                    chromosome.set(i, !chromosome.get(i));
                }
            }
            c++;
        }
    }

    protected List<Double> evaluation(List<Solution> population, Function function, GAResolution gaResolution) {
        List<Double> fitnessScores = new ArrayList<>(population.size());
        double scoreSum = 0;
//        Solution worstSolution = null;
//        double worstFitness = Double.MAX_VALUE;
        double bestFitness = 0;
        Solution bestSolution = null;
        for(Solution chromosome : population) {
            double fitnessScore = fitnessFunction.calculateScore(function, getPhenotypeFrom(chromosome, function.getProblemParameters()));
            scoreSum += function.evaluate(getPhenotypeFrom(chromosome, function.getProblemParameters()));
            fitnessScores.add(fitnessScore);
            if(fitnessScore > bestFitness) {
                bestFitness = fitnessScore;
                bestSolution = chromosome;
            }
//            if(fitnessScore < worstFitness) {
//                worstFitness = fitnessScore;
//                worstSolution = chromosome;
//            }
        }
//        if(bestFitness > gaResolution.getBestFitnessScoreEver()) {
//            gaResolution.setBestFitnessScoreEver(bestFitness);
//        }
//        else {
//            bestSolution = gaResolution.getBestSolutions().get(gaResolution.getBestSolutions().size()-1).clone();
//            int toRemoveIndex = ThreadLocalRandom.current().nextInt(population.size());
//            Solution toRemoveChromosome = population.get(toRemoveIndex);
//            population.remove(toRemoveChromosome);
//            population.add(bestSolution);
//            scoreSum -= function.evaluate(getPhenotypeFrom(chromosome, function.getProblemParameters()));
//            scoreSum += gaResolution.getBestFitnessScoreEver();
//        }
        gaResolution.addBestSolution(bestSolution.clone());
        gaResolution.setScoreAvg(scoreSum/fitnessScores.size());
        return fitnessScores;
    }

    protected List<Solution> initialization(int populationSize, ProblemParameters problemParameters) {
        List<Solution> population = new ArrayList<>(populationSize);
        for (int i = 0; i < populationSize; i++) {
            population.add(generateRandomCandidate(problemParameters));
        }
        return population;
    }
}
