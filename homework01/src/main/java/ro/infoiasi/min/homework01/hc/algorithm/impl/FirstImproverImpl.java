package ro.infoiasi.min.homework01.hc.algorithm.impl;

import ro.infoiasi.min.homework01.hc.algorithm.Improver;
import ro.infoiasi.min.homework01.hc.exception.NotImprovedException;
import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.Solution;

import java.util.*;

import static ro.infoiasi.min.homework01.hc.util.Utils.getPhenotypeFrom;

/**
 * Created by lucian on 05.03.2017.
 */
public class FirstImproverImpl implements Improver {

    private boolean maximize;

    private Random random = new Random();

    public FirstImproverImpl(boolean maximize) {
        this.maximize = maximize;
    }

    public Solution improve(Solution candidate, Function function, SortedMap<Integer, Double> bestGenerationScores) throws NotImprovedException {
        double bestScore = function.evaluate(getPhenotypeFrom(candidate, function.getProblemParameters()));

        List<Integer> neighbors = new ArrayList<>();
        for (int i = 0; i < getSize(candidate.getNs()); i++) {
            neighbors.add(i);
        }
        int searchCount = 0;
        while(neighbors.size() > 0) {
            searchCount++;
            Solution neighbor = generateNeighbor(neighbors, candidate);
            double currentScore = function.evaluate(getPhenotypeFrom(neighbor, function.getProblemParameters()));
            if(maximize) {
                if(bestScore < currentScore) {
                    bestGenerationScores.put(bestGenerationScores.lastKey()+searchCount, currentScore);
                    return neighbor;
                }
            }
            else {
                if(bestScore > currentScore) {
                    bestGenerationScores.put(bestGenerationScores.lastKey()+searchCount, currentScore);
                    return neighbor;
                }
            }

        }
        throw new NotImprovedException();
    }

    private int getSize(int[] ns) {
        int size = 0;
        for(int n : ns) {
            size += n;
        }
        return size;
    }

    private Solution generateNeighbor(List<Integer> neighbors, Solution candidate) {
        int randomIndex = random.nextInt(neighbors.size());
        BitSet chromosome = (BitSet) candidate.getGenotype().clone();
        chromosome.set(neighbors.get(randomIndex), !chromosome.get(neighbors.get(randomIndex)));
        neighbors.remove(randomIndex);
        return new Solution(chromosome, candidate.getNs());
    }
}
