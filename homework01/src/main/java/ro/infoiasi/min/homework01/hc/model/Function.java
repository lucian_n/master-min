package ro.infoiasi.min.homework01.hc.model;

/**
 * Created by lucian on 01.03.2017.
 */
public abstract class Function {
    protected ProblemParameters problemParameters;

    public ProblemParameters getProblemParameters() {
        return problemParameters;
    }

    public abstract double evaluate(double[] input);

}
