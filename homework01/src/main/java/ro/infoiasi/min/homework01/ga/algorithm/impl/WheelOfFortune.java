package ro.infoiasi.min.homework01.ga.algorithm.impl;

import ro.infoiasi.min.homework01.ga.algorithm.SelectionAlgorithm;
import ro.infoiasi.min.homework01.hc.model.Solution;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by lucian on 05.03.2017.
 */
public class WheelOfFortune implements SelectionAlgorithm {

    @Override
    public List<Solution> select(List<Solution> solutions, List<Double> fitnessScores) {
        List<Solution> nextGeneration = new ArrayList<>(solutions.size());
        double totalScore = 0;
        for(double fitnessScore : fitnessScores) {
            totalScore += fitnessScore;
        }
        List<Double> probabilities = new ArrayList<>(solutions.size());
        for (double fitnessScore : fitnessScores) {
            probabilities.add(fitnessScore/totalScore);
        }
        for (int i = 0; i < solutions.size(); i++) {
            double randomValue = ThreadLocalRandom.current().nextDouble(0, 1);
            double currentScore = 0;
            for (int j = 0; j < probabilities.size(); j++) {
                currentScore += probabilities.get(j);
                if(randomValue <= currentScore) {
                    nextGeneration.add(solutions.get(j).clone());
                    break;
                }
            }
        }
        return nextGeneration;
    }

}
