package ro.infoiasi.min.homework01.hc.configuration;

/**
 * Created by lucian on 01.03.2017.
 */
public class Configuration {
    public static final int precision = 5;
    public static final int HC_NUMBER_OF_ITERATIONS = 30;
    public static final int POPULATION_SIZE = 100;
    public static final double CROSS_OVER_PROBABILITY = 0.25;
    public static final double AVOID_ZERO_DIVIDE_EPSILON = 0.01;
    public static final double SIX_HUMP_CAMEL_BACK_EPSILON = 1.0317;
    public static final double MUTATION_PROBABILITY = 0.0004;
    public static final int MAX_GENERATION_NUMBER = 500;
    public static final double HC_PROBABILITY = 0.1;
    public static final double HC_PROBABILITY_STEP = 0.0005;
}
