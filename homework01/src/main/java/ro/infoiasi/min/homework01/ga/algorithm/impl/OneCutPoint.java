package ro.infoiasi.min.homework01.ga.algorithm.impl;

import ro.infoiasi.min.homework01.ga.algorithm.CrossoverAlgorithm;
import ro.infoiasi.min.homework01.hc.model.Solution;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

/**
 * Created by lucian on 05.03.2017.
 */
public class OneCutPoint implements CrossoverAlgorithm{

    @Override
    public void crossover(List<Solution> population, double crossoverProbability) {
        TreeMap<Double, Solution> crossoverSelection = new TreeMap<>();
        Solution chromosomeNotAllowed = null;
        double minPermutationNotAllowed = Double.MAX_VALUE;
        for(Solution specimen : population) {
            double randomValue = ThreadLocalRandom.current().nextDouble(0, 1);
            if(randomValue < crossoverProbability) {
                crossoverSelection.put(randomValue, specimen);
            }
            else  {
                if(randomValue < minPermutationNotAllowed) {
                    minPermutationNotAllowed = randomValue;
                    chromosomeNotAllowed = specimen;
                }
            }
        }
        if(crossoverSelection.size() % 2 == 1) {
            boolean remove = ThreadLocalRandom.current().nextBoolean();
            if(remove) {
                crossoverSelection.remove(crossoverSelection.lastKey());
            }
            else {
                crossoverSelection.put(minPermutationNotAllowed, chromosomeNotAllowed);
            }
        }
        int n = IntStream.of(population.get(0).getNs()).sum();
        while (!crossoverSelection.isEmpty()) {
            int cutPoint = ThreadLocalRandom.current().nextInt(1, n);
            BitSet chromosome1 = crossoverSelection.pollFirstEntry().getValue().getGenotype();
            BitSet chromosome2 = crossoverSelection.pollFirstEntry().getValue().getGenotype();
            for (int j = cutPoint; j < n; j++) {
                boolean aux = chromosome1.get(j);
                chromosome1.set(j, chromosome2.get(j));
                chromosome2.set(j, aux);
            }
        }
    }
}
