package ro.infoiasi.min.homework01.hc.algorithm;

import ro.infoiasi.min.homework01.hc.exception.NotImprovedException;
import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.ProblemParameters;
import ro.infoiasi.min.homework01.hc.model.Solution;

import java.util.*;

import static ro.infoiasi.min.homework01.hc.util.Utils.getPhenotypeFrom;

/**
 * Created by lnevoe on 2/27/2017.
 */
public class HC {

    private Improver improver;

    private SortedMap<Integer, Double> bestGenerationScores;

    public HC(Improver improver) {
        this.improver = improver;
    }

    public Solution run(Function function, Solution candidate) {
        bestGenerationScores = new TreeMap<>();
        bestGenerationScores.put(0, function.evaluate(getPhenotypeFrom(candidate, function.getProblemParameters())));
        int i=0;
        try {
            while (true) {
                i++;
                candidate =  improver.improve(candidate, function, bestGenerationScores);
            }
        } catch (NotImprovedException e) {
            System.out.println(i);
            return candidate;
        }
    }

    public SortedMap<Integer, Double> getBestGenerationScores() {
        return bestGenerationScores;
    }
}
