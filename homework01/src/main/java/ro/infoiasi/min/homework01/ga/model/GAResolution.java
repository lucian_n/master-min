package ro.infoiasi.min.homework01.ga.model;

import ro.infoiasi.min.homework01.hc.model.Solution;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucian on 07.03.2017.
 */
public class GAResolution {
    private List<Solution> bestSolutions;
    private List<Double> scoreAvg;
    private double bestFitnessScoreEver = 0;

    public GAResolution() {
        bestSolutions = new ArrayList<>();
        scoreAvg = new ArrayList<>();
    }

    public void addBestSolution(Solution solution) {
        bestSolutions.add(solution);
    }

    public void setScoreAvg(double fitnessAvg) {
        this.scoreAvg.add(fitnessAvg);
    }

    public List<Solution> getBestSolutions() {
        return bestSolutions;
    }

    public List<Double> getScoreAvg() {
        return scoreAvg;
    }

    public double getBestFitnessScoreEver() {
        return bestFitnessScoreEver;
    }

    public void setBestFitnessScoreEver(double bestFitnessScoreEver) {
        this.bestFitnessScoreEver = bestFitnessScoreEver;
    }
}
