package ro.infoiasi.min.homework01.hc.algorithm.impl;

import ro.infoiasi.min.homework01.hc.algorithm.Improver;
import ro.infoiasi.min.homework01.hc.exception.NotImprovedException;
import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.Solution;

import java.util.*;

import static ro.infoiasi.min.homework01.hc.configuration.Configuration.POPULATION_SIZE;
import static ro.infoiasi.min.homework01.hc.util.Utils.getPhenotypeFrom;

/**
 * Created by lucian on 01.03.2017.
 */
public class BestImproverImpl implements Improver {

    private boolean maximize;

    public BestImproverImpl(boolean maximize) {
        this.maximize = maximize;
    }

    public Solution improve(Solution candidate, Function function, SortedMap<Integer, Double> bestGenerationScores) throws NotImprovedException {
        List<Solution> neighborhood = generateNeighborhood(candidate);
        Solution bestSolution = candidate;
        double bestScore = function.evaluate(getPhenotypeFrom(candidate, function.getProblemParameters()));
        for(Solution neighbor : neighborhood) {
            double currentScore = function.evaluate(getPhenotypeFrom(neighbor, function.getProblemParameters()));
            if(maximize) {
                if(bestScore < currentScore) {
                    bestScore = currentScore;
                    bestSolution = neighbor;
                }
            }
            else {
                if(bestScore > currentScore) {
                    bestScore = currentScore;
                    bestSolution = neighbor;
                }
            }
        }
        if(bestSolution == candidate) {
            throw new NotImprovedException();
        }

        int sum = 0;
        for(int a : candidate.getNs()) {
            sum += a;
        }
        bestGenerationScores.put(bestGenerationScores.lastKey() + sum, bestScore);
        return bestSolution;
    }

    private List<Solution> generateNeighborhood(Solution candidate) {
        List<Solution> result = new ArrayList<Solution>();
        for (int i = 0; i < candidate.getGenotype().size(); i++) {
            BitSet chromosome = (BitSet) candidate.getGenotype().clone();
            chromosome.set(i, !chromosome.get(i));
            result.add(new Solution(chromosome, candidate.getNs()));
        }
        return result;
    }


}
