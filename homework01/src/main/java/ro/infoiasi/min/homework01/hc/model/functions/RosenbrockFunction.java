package ro.infoiasi.min.homework01.hc.model.functions;

import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.ProblemParameters;

import static java.lang.Math.pow;
import static ro.infoiasi.min.homework01.hc.configuration.Configuration.AVOID_ZERO_DIVIDE_EPSILON;
import static ro.infoiasi.min.homework01.hc.util.Utils.toPrecision;

/**
 * Created by lucian on 04.03.2017.
 */
public class RosenbrockFunction extends Function {

    public RosenbrockFunction(int n) {
        problemParameters = new ProblemParameters(n, 2.048f, -2.048f, AVOID_ZERO_DIVIDE_EPSILON);
    }

    public double evaluate(double[] input) {
        double result = 0;
        for (int i = 0; i < input.length-1; i++) {
            result += 100*pow(input[i+1] - pow(input[i],2),2) + pow(1-input[i],2);
        }
        return result;
    }
}
