package ro.infoiasi.min.homework01.hc.model.functions;

import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.ProblemParameters;

import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static ro.infoiasi.min.homework01.hc.configuration.Configuration.SIX_HUMP_CAMEL_BACK_EPSILON;
import static ro.infoiasi.min.homework01.hc.util.Utils.toPrecision;

/**
 * Created by lucian on 04.03.2017.
 */
public class SixHumpCamelBackFunction extends Function {

    public SixHumpCamelBackFunction() {
        double[] inputMaxValues = {3,2};
        double[] inputMinValues = {-3,-2};
        problemParameters = new ProblemParameters(2, inputMaxValues, inputMinValues, SIX_HUMP_CAMEL_BACK_EPSILON);
    }

    public double evaluate(double[] input) {
        double x1 = input[0];
        double x2 = input[1];
        double result = (4 - 2.1*pow(x1, 2) + pow(abs(x1), 4)/3) * pow(x1,2);
        result += x1 * x2 + (-4 + 4 * pow(x2,2)) * pow(x2,2);
        return result;
    }
}
