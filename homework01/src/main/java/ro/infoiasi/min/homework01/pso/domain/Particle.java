package ro.infoiasi.min.homework01.pso.domain;

import java.util.List;

/**
 * @author <a href="mailto:nevol@amazon.com">nevol</a>
 */
public class Particle {
    private List<Double> values;
    private List<Double> velocity;
    private List<Double> personalBestPos;
    private double bestScore;

    public double getBestScore() {
        return bestScore;
    }

    public void setBestScore(double bestScore) {
        this.bestScore = bestScore;
    }

    public List<Double> getValues() {
        return values;
    }

    public void setValues(List<Double> values) {
        this.values = values;
    }

    public List<Double> getVelocity() {
        return velocity;
    }

    public void setVelocity(List<Double> velocity) {
        this.velocity = velocity;
    }

    public List<Double> getPersonalBestPos() {
        return personalBestPos;
    }

    public void setPersonalBestPos(List<Double> personalBestPos) {
        this.personalBestPos = personalBestPos;
    }

    @Override
    public String toString() {
        return "values=" + values ;
    }
}
