package ro.infoiasi.min.homework01.hc.model.functions;

import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.ProblemParameters;

import static java.lang.Math.pow;
import static ro.infoiasi.min.homework01.hc.configuration.Configuration.AVOID_ZERO_DIVIDE_EPSILON;

/**
 * Created by nevol on 4/1/17.
 */
public class UnnamedFunction extends Function {

    public UnnamedFunction() {
        problemParameters = new ProblemParameters(1, 31, 0, AVOID_ZERO_DIVIDE_EPSILON);
    }

    @Override
    public double evaluate(double[] input) {
        double x = input[0];
        return pow(x, 3)-60*pow(x,2)+900*x+100;
    }
}
