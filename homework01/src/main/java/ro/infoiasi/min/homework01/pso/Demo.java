package ro.infoiasi.min.homework01.pso;

import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.functions.RastriginFunction;
import ro.infoiasi.min.homework01.pso.algorithm.PSO;

import java.io.IOException;

/**
 * @author <a href="mailto:nevol@amazon.com">nevol</a>
 */
public class Demo {
    public static void main(String[] args) throws IOException {
        Function rastrigin = new RastriginFunction(30);
        PSO pso = new PSO();
        pso.run(rastrigin);
    }
}
