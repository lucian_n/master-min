package ro.infoiasi.min.homework01.pso.configuration;

/**
 * @author <a href="mailto:nevol@amazon.com">nevol</a>
 */
public class PSOConfig {
    public static final int C1 = 2;
    public static final int C2 = 2;
    public static final double W = 1;

}
