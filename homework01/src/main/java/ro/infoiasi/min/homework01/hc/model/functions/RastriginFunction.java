package ro.infoiasi.min.homework01.hc.model.functions;

import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.ProblemParameters;

import static ro.infoiasi.min.homework01.hc.configuration.Configuration.AVOID_ZERO_DIVIDE_EPSILON;
import static ro.infoiasi.min.homework01.hc.util.Utils.toPrecision;

/**
 * Created by lnevoe on 3/3/2017.
 */
public class RastriginFunction extends Function {

    public RastriginFunction(int domainDimension) {
        problemParameters = new ProblemParameters(domainDimension, 5.12f, -5.12f, AVOID_ZERO_DIVIDE_EPSILON);
    }

    public double evaluate(double[] input) {
        double result = 10*input.length;
        for (int i = 0; i < input.length; i++) {
            result += Math.pow(input[i], 2) - 10 * Math.cos(2*Math.PI*input[i]);
        }
       // return toPrecision(result);
        return result;
    }
}
