package ro.infoiasi.min.homework01.hc.model;

import java.util.BitSet;
import java.util.stream.IntStream;

/**
 * Created by lnevoe on 2/27/2017.
 */
public class Solution implements Cloneable{
    private BitSet genotype;
    private int[] ns;

    public Solution(BitSet genotype, int[] ns) {
        this.genotype = genotype;
        this.ns = ns;
    }

    public int[] getNs() {
        return ns;
    }

    public BitSet getGenotype() {
        return genotype;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        int n = IntStream.of(ns).sum();
        for (int i = 0; i < n; i++) {
            builder.append(genotype.get(i) ? 1 : 0);
        }
        return builder.toString();
    }

    public Solution clone() {
        return new Solution((BitSet) genotype.clone(), ns.clone());
    }
}
