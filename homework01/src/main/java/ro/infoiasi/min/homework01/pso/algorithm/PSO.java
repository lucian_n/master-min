package ro.infoiasi.min.homework01.pso.algorithm;

import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.ProblemParameters;
import ro.infoiasi.min.homework01.pso.domain.Particle;
import ro.infoiasi.min.homework01.pso.domain.Particles;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static ro.infoiasi.min.homework01.hc.configuration.Configuration.POPULATION_SIZE;
import static ro.infoiasi.min.homework01.pso.configuration.PSOConfig.*;

/**
 * Created by nevol on 4/3/17.
 */
public class PSO {

    static final double SIGMA = C1 + C2;
    static final double X = 2/abs(2- SIGMA -sqrt(pow(SIGMA,2)-4* SIGMA));

    public List<Double> run(Function function) throws IOException {
        int t = 0;
        Particles particles = init(function);
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(new File("pso.data")));

        double w = W;
        while (t < 1000) {
            w-=0.001;
            t++;
            double avg = 0;
            for(Particle particle : particles.getParticles()) {
                for (int i = 0; i < particle.getValues().size(); i++) {
                    double r1 = ThreadLocalRandom.current().nextDouble();
                    double r2 = ThreadLocalRandom.current().nextDouble();
                    double r0 = ThreadLocalRandom.current().nextDouble();
                    double x = particle.getValues().get(i);
                    double pi = particle.getPersonalBestPos().get(i);
                    double gi = particles.getBestPos().get(i);
                    double vi = particle.getVelocity().get(i);
                    vi = X*(w*r0*vi + C1*r1*(pi-x) + C2*r2*(gi-x));
                    particle.getVelocity().set(i, vi);
                    x += vi;
                    if(x > function.getProblemParameters().getInputMaxValues()[i]) {
                        x = function.getProblemParameters().getInputMaxValues()[i];
                    }
                    if(x < function.getProblemParameters().getInputMinValues()[i]) {
                        x = function.getProblemParameters().getInputMinValues()[i];
                    }
                    particle.getValues().set(i, x);
                }
                double currentScore = function.evaluate(particle.getValues().stream().mapToDouble(Double::doubleValue).toArray());
                avg += currentScore;
                double bestPersonalScore = particle.getBestScore();
                if(currentScore < bestPersonalScore) {
                    particle.setPersonalBestPos(new ArrayList<>(particle.getValues()));
                    particle.setBestScore(currentScore);
                    if(currentScore < particles.getBestScore()) {
                        particles.setBestScore(currentScore);
                        particles.setBestPos(new ArrayList<>(particle.getValues()));
                    }
                }
            }
            System.out.println(String.format("Generation %d: \n \tBest score %f \n \t avg score %f",t,particles.getBestScore(), avg/POPULATION_SIZE));
            for (Particle particle : particles.getParticles()) {
                fileWriter.write(particle.getValues().get(0) + " " + particle.getValues().get(1));
                fileWriter.newLine();
            }

        }
        fileWriter.flush();
        fileWriter.close();
        return particles.getBestPos();
    }

    private Particles init(Function function) {
        Particles result = new Particles();
        List<List<Double>> particles = generateParticles(function.getProblemParameters());
        List<List<Double>> v = generateVelocities(function.getProblemParameters());
        List<List<Double>> p = initPersonalBestPositions(particles);
        List<Double> g = initGroupBestPosition(function, p);
        result.setBestPos(new ArrayList<>(g));
        result.setBestScore(function.evaluate(g.stream().mapToDouble(Double::doubleValue).toArray()));
        for (int i = 0; i < POPULATION_SIZE; i++) {
            Particle particle = new Particle();
            particle.setPersonalBestPos(new ArrayList<>(p.get(i)));
            particle.setValues(particles.get(i));
            particle.setVelocity(v.get(i));
            particle.setBestScore(function.evaluate(p.get(i).stream().mapToDouble(Double::doubleValue).toArray()));
            result.add(particle);
        }
        return result;
    }

    private List<Double> initGroupBestPosition(Function function, List<List<Double>> p) {
        List<Double> g = p.get(0);
        double avg, bestScore = avg = function.evaluate(g.stream().mapToDouble(Double::doubleValue).toArray());
        for (int i = 1; i < POPULATION_SIZE; i++) {
            double score = function.evaluate(p.get(i).stream().mapToDouble(Double::doubleValue).toArray());
            avg += score;
            if(score < bestScore) {
                bestScore = score;
                g = p.get(i);
            }
        }
        System.out.println(String.format("Generation 0: \n \tBest score %f \n \t avg score %f",bestScore, avg/POPULATION_SIZE));
        return g;
    }

    private List<List<Double>> initPersonalBestPositions(List<List<Double>> particles) {
        List<List<Double>> p = new ArrayList<>();
        p.addAll(particles);
        return p;
    }

    private List<List<Double>> generateParticles(ProblemParameters problemParameters) {
        List<List<Double>> result = new ArrayList<>();
        for (int i = 0; i < POPULATION_SIZE; i++) {
            List<Double> particle = new ArrayList<>();
            for (int j = 0; j < problemParameters.getDomainDimension(); j++) {
                double min = problemParameters.getInputMinValues()[j];
                double max = problemParameters.getInputMaxValues()[j];
                particle.add(ThreadLocalRandom.current().nextDouble(min, max));
            }
            result.add(particle);
        }
        return result;
    }

    private List<List<Double>> generateVelocities(ProblemParameters problemParameters) {
        List<List<Double>> result = new ArrayList<>();
        for (int i = 0; i < POPULATION_SIZE; i++) {
            List<Double> velocity = new ArrayList<>();
            for (int j = 0; j < problemParameters.getDomainDimension(); j++) {
                double min = problemParameters.getInputMinValues()[j];
                double max = problemParameters.getInputMaxValues()[j];
                double aux = max-min;

                velocity.add(ThreadLocalRandom.current().nextDouble(-abs(aux), abs(aux)));
            }
            result.add(velocity);
        }
        return result;
    }
}
