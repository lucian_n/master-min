package ro.infoiasi.min.homework01.ga.fitness;

import ro.infoiasi.min.homework01.hc.model.Function;

/**
 * Created by lucian on 05.03.2017.
 */
public interface FitnessFunction {

    double calculateScore(Function function, double[] input);
}
