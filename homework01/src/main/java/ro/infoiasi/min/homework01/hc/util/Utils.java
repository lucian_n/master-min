package ro.infoiasi.min.homework01.hc.util;

import ro.infoiasi.min.homework01.hc.model.ProblemParameters;
import ro.infoiasi.min.homework01.hc.model.Solution;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.BitSet;
import java.util.Random;

import static ro.infoiasi.min.homework01.hc.configuration.Configuration.precision;

/**
 * Created by lnevoe on 3/3/2017.
 */
public class Utils {
    public static double[] getPhenotypeFrom(Solution solution, ProblemParameters problemParameters) {
        double[] phenotype = new double[problemParameters.getDomainDimension()];
        BitSet genotype = solution.getGenotype();
        for (int i = 0; i < problemParameters.getDomainDimension(); i++) {
            int n = solution.getNs()[i];
            int decimal = bitSetToInt(genotype.get(i * n, (i+1)*n), n);
            double a = problemParameters.getInputMinValues()[i];
            double b = problemParameters.getInputMaxValues()[i];
            phenotype[i] = new BigDecimal(a + decimal*(b-a)/(Math.pow(2, n) -1)).setScale(precision, RoundingMode.HALF_UP).doubleValue();
           /* if(phenotype[i] > 0) {
                System.out.println("hereeee" + phenotype[i]);
            }*/
        }
        return phenotype;
    }

    private static int bitSetToInt(BitSet bitSet, int n)
    {
        int bitInteger = 0;
        for(int i = 0 ; i < n; i++)
            if(bitSet.get(i))
                bitInteger |= (1 << (n-1-i));
        return bitInteger;
    }

    public static Solution generateRandomCandidate(ProblemParameters problemParameters) {
        int[] ns = new int[problemParameters.getDomainDimension()];
        int chromosomeSize = 0;
        for (int i = 0; i < problemParameters.getDomainDimension(); i++) {
            ns[i] =  getRepresentationSize(problemParameters.getInputMinValues()[i], problemParameters.getInputMaxValues()[i]);
            chromosomeSize += ns[i];
        }
        return new Solution(generateRandomChromosome(chromosomeSize), ns);
    }

    private static BitSet generateRandomChromosome(int chromosomeSize) {
        Random random = new Random();
        BitSet randomBitSet = new BitSet(chromosomeSize);
        for (int i = 0; i < chromosomeSize; i++) {
            randomBitSet.set(i, random.nextBoolean());
        }

        return randomBitSet;
    }

    private static int getRepresentationSize(double inputMinValue, double inputMaxValue) {
        int n = (int) Math.ceil((inputMaxValue - inputMinValue) * Math.pow(10, precision));
        return (int) Math.ceil(Math.log(n)/Math.log(2));
    }

    public static double toPrecision(double value) {
        return new BigDecimal(value).setScale(precision, RoundingMode.HALF_UP).doubleValue();
    }
}
