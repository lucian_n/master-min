package ro.infoiasi.min.homework01.hc.model;

import java.util.Arrays;

/**
 * Created by lnevoe on 2/27/2017.
 */
public class ProblemParameters {
    private final int domainDimension;
    private final double[] inputMaxValues;
    private final double[] inputMinValues;
    private final double epsilon;

    public ProblemParameters(int domainDimension, double inputMaxValue, double inputMinValue, double epsilon) {
        this.domainDimension = domainDimension;
        this.inputMaxValues = new double[domainDimension];
        this.inputMinValues = new double[domainDimension];
        this.epsilon = epsilon;
        Arrays.fill(this.inputMaxValues, inputMaxValue);
        Arrays.fill(this.inputMinValues, inputMinValue);
    }

    public ProblemParameters(int domainDimension, double[] inputMaxValues, double[] inputMinValues, double epsilon) {
        this.domainDimension = domainDimension;
        this.inputMaxValues = inputMaxValues;
        this.inputMinValues = inputMinValues;
        this.epsilon = epsilon;
    }

    public int getDomainDimension() {
        return domainDimension;
    }

    public double[] getInputMaxValues() {
        return inputMaxValues;
    }

    public double[] getInputMinValues() {
        return inputMinValues;
    }

    public double getEpsilon() {
        return epsilon;
    }
}
