package ro.infoiasi.min.homework01.hc.algorithm;

import ro.infoiasi.min.homework01.hc.exception.NotImprovedException;
import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.Solution;

import java.util.SortedMap;

/**
 * Created by lnevoe on 2/27/2017.
 */
public interface Improver {
    Solution improve(Solution candidate, Function function, SortedMap<Integer, Double> bestGenerationScores) throws NotImprovedException;
}
