package ro.infoiasi.min.homework01.ga.algorithm;

import ro.infoiasi.min.homework01.hc.model.Solution;

import java.util.List;

/**
 * Created by lucian on 05.03.2017.
 */
public interface SelectionAlgorithm {

    List<Solution> select(List<Solution> solutions, List<Double> fitnessScores);



}
