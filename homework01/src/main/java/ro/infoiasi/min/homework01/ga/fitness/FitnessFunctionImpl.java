package ro.infoiasi.min.homework01.ga.fitness;

import ro.infoiasi.min.homework01.hc.model.Function;

import static ro.infoiasi.min.homework01.hc.util.Utils.toPrecision;

/**
 * Created by lucian on 05.03.2017.
 */
public class FitnessFunctionImpl implements FitnessFunction {

    @Override
    public double calculateScore(Function function, double[] input) {
        double output = function.evaluate(input);
        return 1/(output + function.getProblemParameters().getEpsilon());
    }
}
