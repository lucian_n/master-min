package ro.infoiasi.min.homework01.ga;

import ro.infoiasi.min.homework01.ga.algorithm.GA;
import ro.infoiasi.min.homework01.ga.algorithm.GAPlusHC;
import ro.infoiasi.min.homework01.ga.algorithm.impl.OneCutPoint;
import ro.infoiasi.min.homework01.ga.algorithm.impl.WheelOfFortune;
import ro.infoiasi.min.homework01.ga.fitness.FitnessFunctionImpl;
import ro.infoiasi.min.homework01.ga.model.GAResolution;
import ro.infoiasi.min.homework01.ga.model.GAStatistics;
import ro.infoiasi.min.homework01.hc.algorithm.HC;
import ro.infoiasi.min.homework01.hc.algorithm.impl.BestImproverImpl;
import ro.infoiasi.min.homework01.hc.configuration.Configuration;
import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.functions.GriewangkFunction;
import ro.infoiasi.min.homework01.hc.model.functions.RastriginFunction;
import ro.infoiasi.min.homework01.hc.model.functions.RosenbrockFunction;
import ro.infoiasi.min.homework01.hc.model.functions.SixHumpCamelBackFunction;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static ro.infoiasi.min.homework01.hc.configuration.Configuration.precision;
import static ro.infoiasi.min.homework01.hc.util.Utils.getPhenotypeFrom;

/**
 * Created by lucian on 05.03.2017.
 */
public class Demo {
    public static void main(String[] args) {
        Function rastriginFunction = new RastriginFunction(30);
        Function griewangkFunction = new GriewangkFunction(10);
        Function rosenbrockFunction = new RosenbrockFunction(20);
        Function sixHumpCamelBackFunction = new SixHumpCamelBackFunction();
        GA gaInstance = new GA(new FitnessFunctionImpl(), new WheelOfFortune(), new OneCutPoint());
        HC hc = new HC(new BestImproverImpl(false));
        GA gaPlusHc = new GAPlusHC(new FitnessFunctionImpl(), new WheelOfFortune(), new OneCutPoint(), hc);
        GAStatistics statistics = runIteratedGA(gaInstance, rastriginFunction, 30);
        appendToFile("ga_statistics.csv", statistics);

    }

    private static void appendToFile(String s, GAStatistics gaStatistics) {
        File file = new File(s);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write(gaStatistics.toString() + "\n");
            bw.close();
            fw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static GAStatistics runIteratedGA(GA gaInstance, Function function, int iterations) {
        double bestScore = Double.MAX_VALUE;
        List<Long> executionsTime = new ArrayList<>();
        List<Integer> generationNo = new ArrayList<>();
        List<Double> bestScores = new ArrayList<>();
        for (int i = 0; i < iterations; i++) {
            System.out.println("Iteratia: " + i);
            System.out.println("\tGenerations: ");
            Date startDate = new Date();
            GAResolution gaResolution = gaInstance.run(function);
            executionsTime.add(new Date().getTime() - startDate.getTime());
            generationNo.add(gaResolution.getBestSolutions().size());
            double[] bestSolution = getPhenotypeFrom(gaResolution.getBestSolutions().get(gaResolution.getBestSolutions().size()-1), function.getProblemParameters());
            double currentScore = function.evaluate(bestSolution);
            bestScores.add(currentScore);
            if (bestScore > currentScore) {
                System.out.print("\tSolutions: ");
                for (double input : bestSolution) {
                    BigDecimal bigDecimal = new BigDecimal(input);
                    System.out.print(bigDecimal.setScale(precision, RoundingMode.HALF_UP) + " , ");
                }
                System.out.println("\n\tScore: " + currentScore);
                bestScore = currentScore;
            }
            System.out.println("\n-----------------------------------------------------------------------------------------");
            try {
                BufferedWriter fileWriter = new BufferedWriter(new FileWriter("Ga.data"));
                int j = 0;
                for(Double value : gaResolution.getScoreAvg()) {
                    fileWriter.write(j+" " + value + "\n");
                    j++;
                }
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("best score : " + bestScore);
        GAStatistics gaStatistics = new GAStatistics();
        gaStatistics.setIterations(iterations);
        gaStatistics.setPrecision(Configuration.precision);
        gaStatistics.setCrossoverProbability(Configuration.CROSS_OVER_PROBABILITY);
        gaStatistics.setMutationProbability(Configuration.MUTATION_PROBABILITY);
        gaStatistics.setBestSolution(bestScore);
        gaStatistics.setBestSolutionsAvg(bestScores.stream().mapToDouble(Double::doubleValue).sum() / iterations);
        gaStatistics.setExecutionTimeAvg(executionsTime.stream().mapToLong(Long::longValue).sum() * 1. / iterations);
        gaStatistics.setGenerationNoAvg(generationNo.stream().mapToInt(Integer::intValue).sum() * 1. / iterations);
        gaStatistics.setPopSize(Configuration.POPULATION_SIZE);
        return gaStatistics;
    }
}
