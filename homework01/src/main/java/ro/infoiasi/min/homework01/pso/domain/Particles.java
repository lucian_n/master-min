package ro.infoiasi.min.homework01.pso.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:nevol@amazon.com">nevol</a>
 */
public class Particles {
    private List<Particle> particles;
    private List<Double> bestPos;
    private double bestScore;

    public Particles() {
        this.particles = new ArrayList<>();
    }

    public double getBestScore() {
        return bestScore;
    }

    public void setBestScore(double bestScore) {
        this.bestScore = bestScore;
    }

    public List<Particle> getParticles() {
        return particles;
    }

    public void setParticles(List<Particle> particles) {
        this.particles = particles;
    }

    public List<Double> getBestPos() {
        return bestPos;
    }

    public void setBestPos(List<Double> bestPos) {
        this.bestPos = bestPos;
    }

    public void add(Particle particle) {
        particles.add(particle);
    }

    @Override
    public String toString() {
        return "particles=" + particles;
    }
}
