package ro.infoiasi.min.homework01.hc;

import ro.infoiasi.min.homework01.hc.algorithm.HC;
import ro.infoiasi.min.homework01.hc.algorithm.impl.BestImproverImpl;
import ro.infoiasi.min.homework01.hc.algorithm.impl.FirstImproverImpl;
import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.Solution;
import ro.infoiasi.min.homework01.hc.model.functions.UnnamedFunction;

import java.util.*;

import static ro.infoiasi.min.homework01.hc.util.Utils.generateRandomCandidate;
import static ro.infoiasi.min.homework01.hc.util.Utils.getPhenotypeFrom;

/**
 * Created by nevol on 4/1/17.
 */
public class Demo2 {
    public static void main(String[] args) {
        Function function = new UnnamedFunction();
        HC hcInstance = new HC(new FirstImproverImpl(true));

        Map<Double, Set<Double>> landscape = runHc(hcInstance, function, 200);
        BitSet bitSet = new BitSet(5);
        bitSet.set(1,true);
        int[] ns = {5};
        hcInstance.run(function, new Solution(bitSet, ns));

    }

    private static Map<Double, Set<Double> >runHc(HC hcInstance, Function function, int runNo) {
        Map<Double, Set<Double>> landscape = new HashMap<>();
        for (int i = 0; i < runNo; i++) {
            Solution candidate = generateRandomCandidate(function.getProblemParameters());
            Solution best = hcInstance.run(function,candidate);
            double bestSolution = getPhenotypeFrom(best, function.getProblemParameters())[0];
            Set<Double> values = landscape.getOrDefault(bestSolution, new TreeSet<>());
            values.add(getPhenotypeFrom(candidate, function.getProblemParameters())[0]);
            landscape.put(bestSolution, values);
        }
        return landscape;
    }
}
