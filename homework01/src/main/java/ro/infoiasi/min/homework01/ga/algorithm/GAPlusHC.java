package ro.infoiasi.min.homework01.ga.algorithm;

import ro.infoiasi.min.homework01.ga.fitness.FitnessFunction;
import ro.infoiasi.min.homework01.ga.model.GAResolution;
import ro.infoiasi.min.homework01.hc.algorithm.HC;
import ro.infoiasi.min.homework01.hc.model.Function;
import ro.infoiasi.min.homework01.hc.model.Solution;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static ro.infoiasi.min.homework01.hc.configuration.Configuration.*;

/**
 * Created by nevol on 3/21/17.
 */
public class GAPlusHC extends GA {

    private HC hc;

    private double hcProb = HC_PROBABILITY;

    public GAPlusHC(FitnessFunction fitnessFunction, SelectionAlgorithm selectionAlgorithm, CrossoverAlgorithm crossoverAlgorithm, HC hc) {
        super(fitnessFunction, selectionAlgorithm, crossoverAlgorithm);
        this.hc = hc;
    }

    @Override
    public GAResolution run(Function function) {
        int generation = 0;
        GAResolution gaResolution = new GAResolution();
        List<Solution> population = initialization(POPULATION_SIZE, function.getProblemParameters());
        List<Double> fitnessScores = evaluation(population, function, gaResolution);
        while (!converge(population) && generation < MAX_GENERATION_NUMBER) {
            generation++;
            population = selectionAlgorithm.select(population, fitnessScores);
            mutation(population, MUTATION_PROBABILITY);
            crossoverAlgorithm.crossover(population, CROSS_OVER_PROBABILITY);
            applyHC(population, function);/**/
            fitnessScores = evaluation(population, function, gaResolution);
            logStatistics(gaResolution, generation, function);
            if(generation%5 == 0) {
                hcProb -= HC_PROBABILITY_STEP;
            }
        }

        return gaResolution;
    }

    private void applyHC(List<Solution> population, Function function) {
        for (int i = 0; i < population.size(); i++) {
            double randomValue = ThreadLocalRandom.current().nextDouble(0, 1);
            if(randomValue <= hcProb) {
                population.set(i, hc.run(function, population.get(i)));
            }
        }
    }
}
