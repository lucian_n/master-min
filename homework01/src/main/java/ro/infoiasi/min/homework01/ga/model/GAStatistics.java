package ro.infoiasi.min.homework01.ga.model;

/**
 * Created by lucian on 10.03.2017.
 */
public class GAStatistics {
    private int iterations;
    private double crossoverProbability;
    private double mutationProbability;
    private int precision;
    private double generationNoAvg;
    private double executionTimeAvg;
    private double bestSolutionsAvg;
    private double bestSolution;
    private int popSize;

    public void setPopSize(int popSize) {
        this.popSize = popSize;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public double getCrossoverProbability() {
        return crossoverProbability;
    }

    public void setCrossoverProbability(double crossoverProbability) {
        this.crossoverProbability = crossoverProbability;
    }

    public double getMutationProbability() {
        return mutationProbability;
    }

    public void setMutationProbability(double mutationProbability) {
        this.mutationProbability = mutationProbability;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    public double getGenerationNoAvg() {
        return generationNoAvg;
    }

    public void setGenerationNoAvg(double generationNoAvg) {
        this.generationNoAvg = generationNoAvg;
    }

    public double getExecutionTimeAvg() {
        return executionTimeAvg;
    }

    public void setExecutionTimeAvg(double executionTimeAvg) {
        this.executionTimeAvg = executionTimeAvg;
    }

    public double getBestSolutionsAvg() {
        return bestSolutionsAvg;
    }

    public void setBestSolutionsAvg(double bestSolutionsAvg) {
        this.bestSolutionsAvg = bestSolutionsAvg;
    }

    public double getBestSolution() {
        return bestSolution;
    }

    public void setBestSolution(double bestSolution) {
        this.bestSolution = bestSolution;
    }

    @Override
    public String toString() {
        return iterations +
                "," + popSize +
                "," + crossoverProbability +
                "," + mutationProbability +
                "," + precision +
                "," + generationNoAvg +
                "," + executionTimeAvg +
                "," + bestSolutionsAvg +
                "," + bestSolution;
    }
}
